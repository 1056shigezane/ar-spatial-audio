//
//  Extensions.swift
//  Spatial Audio Interface
//
//  Created by Narumi Inada on 2021/06/10.
//

import Foundation

import SceneKit


extension SCNGeometry {
    static func randomPrimitive(scale:CGFloat) -> SCNGeometry {
        
        let rand = Int.random(in: 0 ... 7)
        
        switch rand {
        case 0:
            return SCNBox(width: scale, height: scale, length: scale, chamferRadius: 0)
            
        case 1:
            return SCNCapsule(capRadius: scale / 2, height: scale * 1.618)
            
        case 2:
            return SCNCone(topRadius: 0, bottomRadius: scale, height: scale * 2.0)
            
        case 3:
            return SCNCylinder(radius: scale, height: scale * 1.618)
            
        case 4:
            return SCNPyramid(width: scale, height: scale, length: scale)
            
        case 5:
            return SCNSphere(radius: scale)
            
        case 6:
            return SCNTorus(ringRadius: scale, pipeRadius: scale * 0.382)
            
        case 7:
            return SCNTube(innerRadius: scale, outerRadius: scale * 1.192, height: scale)
            
        default:
            return SCNSphere(radius: scale)
            
        }
        
        
        
    }
    
    static func text(string:String, extrude:CGFloat,size:CGFloat) -> SCNText {
        let lineSpaceStyle = NSMutableParagraphStyle()
        lineSpaceStyle.lineSpacing = 20

        
        let attributes: [NSAttributedString.Key : Any] = [
            .font : UIFont.systemFont(ofSize: size),
            .foregroundColor : UIColor.orange,
            .strokeColor : UIColor.red,
            .paragraphStyle: lineSpaceStyle
        ]
        
        let attr = NSAttributedString(string: string, attributes: attributes)
        return SCNText(string: attr, extrusionDepth: extrude)
    }
}
