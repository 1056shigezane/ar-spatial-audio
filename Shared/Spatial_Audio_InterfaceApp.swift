//
//  Spatial_Audio_InterfaceApp.swift
//  Shared
//
//  Created by Narumi Inada on 2021/06/09.
//

import SwiftUI

@main
struct Spatial_Audio_InterfaceApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
