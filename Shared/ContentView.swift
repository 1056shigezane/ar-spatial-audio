//
//  ContentView.swift
//  Shared
//
//  Created by Narumi Inada on 2021/06/09.
//

import SwiftUI

import ARKit
import SceneKit
import RealityKit

#if os(iOS)
import UIKit

final class ARViewRepresentable : ARSCNView {
    
}

extension ARViewRepresentable : UIViewRepresentable {
    func makeUIView(context: Context) -> ARSCNView {
        self
    }
    
    func updateUIView(_ uiView: ARSCNView, context: Context) {
        
    }
    
    typealias UIViewType = ARSCNView
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        if let touchLocation = touches.first?.location(in: self),
//                    let hit = self.hitTest(touchLocation, types: .featurePoint).first {
//                    let anchor = ARAnchor(transform: hit.worldTransform)
//                    let node = self.node(for: anchor)
//                    node?.removeFromParent()
//                }
        guard let touchLocation = touches.first?.location(in: self) else{
            print("NO LOCATION")
            return}
        
        
        let hit = hitTest(touchLocation, types: .existingPlane)
        
        print("Hit Count",hit.count)
        hit.forEach { res in
            //let anchor = ARAnchor(transform: res.worldTransform)
            
            if let anchor = res.anchor,let node = node(for: anchor) {
                print("Node found",node.name)
                node.presentation.removeFromParentNode()
                
                
            }
        }
        
        //            if let anchor = hit.anchor, let node = node(for: anchor) {
        //                node.removeFromParentNode()
        //                print("Remove From Parent")
        //
        //
        //
        //
        //            }else{
//                print("No Hit")
//            }
            
        
        //let hit = hitTest(touchLocation, types: .featurePoint).first
        if let query = raycastQuery(from: touchLocation, allowing: .estimatedPlane, alignment: .any){
            
            let ress : [ARRaycastResult] = session.raycast(query)
            
            guard !ress.isEmpty else{return}
            
            print("YATTA!")
            
            
            ress.forEach { res in
//                let anchor = AnchorEntity(raycastResult: res)
//                res.target
                if let anchor = res.anchor {
                    print("MOTTO YATTA")
                    if let node = node(for: anchor) {
                        print("YATTAAAAA!!!")
                        node.removeFromParentNode()
                    }
                }
            }
            
            
         //   node.removeFromParentNode()
        }
        
        //scene.rootNode.hitTestWithSegment(from: came, to: <#T##SCNVector3#>, options: <#T##[String : Any]?#>)
        
            
//            .first {
//                    let anchor = ARAnchor(transform: hit.worldTransform)
//                    let node = sceneView.node(for: anchor)
//                    node?.removeFromParent()
//                }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
    }
    
}
#else

import AppKit

final class ARViewRepresentable : SCNView {
    
}

extension ARViewRepresentable : NSViewRepresentable {
    func makeUIView(context: Context) -> ARSCNView {
        self
    }
    
    func updateUIView(_ uiView: ARSCNView, context: Context) {
        
    }
    
    typealias UIViewType = ARSCNView
    
    
    
}

#endif

/**
 Spehreのテクスチャーをインナーカメラにしたいよ
 */

import AVFoundation
import CoreMotion

class Shared : NSObject {
    static let shared = Shared()
    
    let view : ARViewRepresentable
    
    var trackingNode : SCNNode = .init()
    
    var hpMotionManager : CMHeadphoneMotionManager = .init()
    var hpMotion : CMAttitude = .init()
    
    let listner : SCNNode = .init()
    private override init() {
        //try! AVAudioSession.sharedInstance().setMode(AVAudioSession.Mode.videoRecording)
        try! AVAudioSession.sharedInstance().setActive(true, options: [])
        
        
        let fpath = Bundle.main.paths(forResourcesOfType: "wav", inDirectory: "Assets.scnassets")[0]
        
        let AP = try! AVAudioPlayer(contentsOf: URL(fileURLWithPath: fpath))
       // AP.play()
        
        
        ap = AP
        view = ARViewRepresentable()
        view.audioEngine.prepare()
        super.init()
        view.backgroundColor = .blue

        let configuration = ARWorldTrackingConfiguration()
        configuration.planeDetection = [.horizontal, .vertical]
        view.delegate = self
        view.session.run(configuration)
        
        
        view.scene.rootNode.addChildNode(trackingNode)
        
        view.isUserInteractionEnabled = true
        
        view.audioEnvironmentNode.distanceAttenuationParameters.maximumDistance = 0.5
        view.audioEnvironmentNode.distanceAttenuationParameters.referenceDistance = 0.0
        view.audioEnvironmentNode.renderingAlgorithm = .HRTFHQ
        view.audioEnvironmentNode.distanceAttenuationParameters.rolloffFactor = 10.0
        
        
        guard let pov = view.pointOfView else{ fatalError() }
        
        
        pov.addChildNode(listner)
        
        try! view.audioEngine.start()
        
        
        hpMotionManager.startDeviceMotionUpdates(to: .main) { mo, err in
            guard let att = mo?.attitude else{return}
            
            if self.isStartOfHPMotion {
                
                self.startAttitude = att
                self.isStartOfHPMotion = false
                
            }
           
            self.hpMotion = att
            
        }
    }
    
    var startAttitude : CMAttitude = .init()
    var ap : AVAudioPlayer
    var isStartOfHPMotion : Bool = true
}

//extension Shared : CMHeadphoneMotionManagerDelegate {
//
//}


extension Shared : ARSCNViewDelegate {
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        guard let planeAnchor = anchor as? ARPlaneAnchor else { return }
        let plane = Plane(anchor: planeAnchor, in: view)
        node.addChildNode(plane)
        
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        // Works Well. but NEEDS refleshing of tracking.
        
        guard !isStartOfHPMotion else{ return }
        
        listner.eulerAngles = .init(hpMotion.pitch - startAttitude.pitch, hpMotion.yaw - startAttitude.yaw, hpMotion.roll - startAttitude.roll)
        
        renderer.audioListener = listner
        
          
    }
}

extension Shared : ARSessionDelegate {
    
}

import CoreHaptics




struct ContentView: View {
    
    
    
    var body: some View {
        
        
        ZStack{
        Shared.shared.view
            
            VStack{
                Spacer()
                
                Button {
                    
                    UIImpactFeedbackGenerator().impactOccurred()
                    
                    let v = Shared.shared.view
                    let scene = Shared.shared.view.scene
                    
                    let sphere = SCNSphere(radius: 0.05)
                    sphere.materials.first?.transparency = 0.4
                    let node = SCNNode(geometry: sphere)
                    let mm = v.session.currentFrame?.camera.transform ?? .init()
                    
                    let files = Bundle.main.paths(forResourcesOfType: "wav", inDirectory: "Assets.scnassets")
                    
                    
                    
                    guard !files.isEmpty else{return}
                    let file = files[Int.random(in: 0 ..< files.count)]
                    
                    print("FILES")
                    print(files)
                    
                    let zero = file.endIndex
                    let end = file.index(zero, offsetBy: -9)
                    let start = file.index(zero, offsetBy: -11)
                    let str = String(file[start ... end])
                    print(str)
                    
                    
                    let geo = SCNGeometry.text(string: str, extrude: 0.5, size: 1.0)
                    geo.firstMaterial?.transparency = 0.4
                    
                    let sph = SCNNode(geometry: geo)
                    sph.rotation = SCNVector4(0,0,1,Float.pi / 2)
                    let scale : Float = 0.01
                    sph.scale = .init(x: scale, y: scale, z: scale)
                    let bx = sph.boundingBox
                    
                    sph.pivot = SCNMatrix4MakeTranslation((bx.max.x - bx.min.x)/2, (bx.max.y - bx.min.y)/2, (bx.max.z - bx.min.z)/2)

                    
                    node.addChildNode(sph)
                    
                    let src = SCNAudioSource(url: URL(fileURLWithPath: file))!
                    
                    src.isPositional = true
                    src.shouldStream = false
                    src.loops = true
                    src.rate = 1.0
                    src.volume = 0.1
                    
                    
                    src.load()
                    let f44 = float4x4(node.transform)

                    
                    
                    let anc = ARAnchor(transform: f44)
                    v.session.add(anchor: anc)

                    
                    scene.rootNode.addChildNode(node)
                    
                    let player = SCNAudioPlayer(source: src)
                    
                    
                    
                    
                    v.audioEnvironmentNode.reverbParameters.level = 40
                    
                    let engine = v.audioEngine
                    if let audioNode = player.audioNode {
                        print("Do Through")
                        engine.attach(audioNode)
                        engine.connect(audioNode, to: v.audioEnvironmentNode, format: v.audioEnvironmentNode.inputFormat(forBus: AVAudioNodeBus(0)))
                        let play = SCNAction.playAudio(src, waitForCompletion: false)
                        node.runAction(play)
                    }
                    
                    
                    //text.transform = SCNMatrix4(mm)
                    node.transform = SCNMatrix4(mm)
                    
                    
                } label: {
                    ZStack{
                        Rectangle().fill().foregroundColor(.gray).frame(width: 100, height: 62, alignment: .center)
                        Text("Add")
                    }
                    
                }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
